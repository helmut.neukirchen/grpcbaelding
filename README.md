# gRPC demo using the Maven build tool

This is based on [https://www.baeldung.com/grpc-introduction](https://www.baeldung.com/grpc-introduction)

It is assumed that the command line build tool [Maven](https://maven.apache.org/install.html) is [installed](https://www.baeldung.com/install-maven-on-windows-linux-mac). 

Also, Java 11 is assumed, but you should be able to change that by searching in 
file `pom.xml` for `11` and replace it by another version. (The reason for using Java 11 is that some still use Ubuntu 20.04 and that comes with a Maven that only works with old Java versions.)

## Getting started

Get the source code:
```
git clone https://gitlab.com/helmut.neukirchen/grpcbaelding.git
```
and change into the main project directory, i.e. the one containg `src`, `pom.xml`, etc.:
```
cd grpcbaelding
```

Then, you have to generate the a fat-jar file containing all the dependencies (and in fact also your own client and server code) in one file using:
 
    mvn clean compile package
 
 (Note that if you use an IDE, such as Eclipse, it looks for source code only in directory `src`. 
 But in fact, generated code (e.g. by running `mvn compile`) will be also placed in `target` 
 where the IDE does not look and hence, the IDE might complain about missing files).
 
 Once you have that fat jar: 
 
    cd target
 
 to change into the directory with the fat-jar, start the server (it will use by default port `8080`, but you can pass a port number as command line parameter) via: 
 
    java -cp grpcBaelding-1.0-SNAPSHOT-jar-with-dependencies.jar com.baeldung.grpc.server.GrpcServer
 
 and in another shell, change there again in to the `target` directory:
 
 
    cd target
 
 and start the client (it will use by default as hostname `localhost` and port `8080`, but you can as optional first command line parameter pass the hostname and as a second optional parameter the port number) via:
 
    java -cp grpcBaelding-1.0-SNAPSHOT-jar-with-dependencies.jar com.baeldung.grpc.client.GrpcClient
 
 Terminate the server, e.g., via CTRL-C in the server terminal window.
 
 **DONE -- THATS ALL!**

 Do not forget to `cd ..` if you want to run, e.g., Maven again. 
 
 ## Listing all dependencies

 To show all dependencies recursively:
 
    mvn dependency:tree

This gives you:    
```
[INFO] --- maven-dependency-plugin:2.8:tree (default-cli) @ grpcBaelding ---
[INFO] is.hi.cs.ds:grpcBaelding:jar:1.0-SNAPSHOT
[INFO] +- io.grpc:grpc-netty-shaded:jar:1.40.1:runtime
[INFO] |  +- com.google.guava:guava:jar:30.1-android:compile
[INFO] |  |  +- com.google.guava:failureaccess:jar:1.0.1:compile
[INFO] |  |  +- com.google.guava:listenablefuture:jar:9999.0-empty-to-avoid-conflict-with-guava:compile
[INFO] |  |  +- org.checkerframework:checker-compat-qual:jar:2.5.5:compile
[INFO] |  |  \- com.google.j2objc:j2objc-annotations:jar:1.3:compile
[INFO] |  +- com.google.errorprone:error_prone_annotations:jar:2.4.0:compile
[INFO] |  +- io.perfmark:perfmark-api:jar:0.23.0:runtime
[INFO] |  \- io.grpc:grpc-core:jar:1.40.1:runtime (version selected from constraint [1.40.1,1.40.1])
[INFO] |     +- com.google.code.gson:gson:jar:2.8.6:runtime
[INFO] |     +- com.google.android:annotations:jar:4.1.1.4:runtime
[INFO] |     \- org.codehaus.mojo:animal-sniffer-annotations:jar:1.19:runtime
[INFO] +- io.grpc:grpc-protobuf:jar:1.40.1:compile
[INFO] |  +- io.grpc:grpc-api:jar:1.40.1:compile
[INFO] |  |  \- io.grpc:grpc-context:jar:1.40.1:compile
[INFO] |  +- com.google.code.findbugs:jsr305:jar:3.0.2:compile
[INFO] |  +- com.google.protobuf:protobuf-java:jar:3.17.2:compile
[INFO] |  +- com.google.api.grpc:proto-google-common-protos:jar:2.0.1:compile
[INFO] |  \- io.grpc:grpc-protobuf-lite:jar:1.40.1:compile
[INFO] +- io.grpc:grpc-stub:jar:1.40.1:compile
[INFO] \- javax.annotation:javax.annotation-api:jar:1.2:compile
```
Use 

    mvn dependency:copy-dependencies
to copy the above JARs into `target/dependency`


## Matching gRPC and protobuf version ##

The gRPC plugin generates code that uses protobufs and therefore the version of gRPC and protobuf need to match. 
Versions known to work are:
    <io.grpc.version>1.40.1</io.grpc.version>
    <protoc.version>3.17.2</protoc.version>

In principle, the reference of working combinations is the gradle file for the gRPC examples, e.g.:
https://github.com/grpc/grpc-java/blob/v1.47.0/examples/build.gradle

But note that while other have 
    <io.grpc.version>1.47.0</io.grpc.version>
    <protoc.version>3.19.2</protoc.version>
working, it does not work for me.

Also note that the mentioned Gradle file has 
    com.google.protobuf:protobuf-java-util:${protobufVersion}
as dependency -- which I do not have, but the comment there says, it is only needed for the advanced version.

## Compiling without Maven

See https://gitlab.com/helmut.neukirchen/grpcManualJava

## Some notes on the modifications made to the example from Baeldung

This is based on [https://www.baeldung.com/grpc-introduction](https://www.baeldung.com/grpc-introduction)

As the source code snippets on that web page does not contain, e.g., `import` statements,
I did download the full code that can be found in GitHub, in the following sub-directory (Git does not allow to check-out only a sub-directory, so you need to have checked-out the whole project and only use that sub-directory):
[https://github.com/eugenp/tutorials/tree/master/grpc](https://github.com/eugenp/tutorials/tree/master/grpc)
 
 Note that this then, however, contains more files than the simple tutorial from the above web page, namely those from
- [Streaming with gRPC in Java](https://www.baeldung.com/java-grpc-streaming)
- [Error Handling in gRPC](https://www.baeldung.com/grpcs-error-handling)
  
 These extra files were deleted then in `src`. 

The original source also assumes some parent POM: few changes needed in `pom.xml` to get rid of it. I did set the Java version to 16 in the POM, but lower versions should work as well.

 Also, a fat-jar generation was added to the POM to ease running the example by having only one JAR with *all* dependencies. 
